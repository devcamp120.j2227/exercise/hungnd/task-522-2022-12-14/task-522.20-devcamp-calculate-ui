import styled from "styled-components";

const ButtonMath = styled.button`
    width: 76px;
    height: 76px;
    font-size: 20px;
    border: 0.5px solid rgb( 58, 70, 85 );
    color: ${props => props.color};
    ${props => props.type && props.type === "normal" ?
        `background-color: rgb(66,80,98); color: white;` :
        `background-color: rgb(64,77,94); color:rgb(173,175,180);`
    }
`

export default ButtonMath;
