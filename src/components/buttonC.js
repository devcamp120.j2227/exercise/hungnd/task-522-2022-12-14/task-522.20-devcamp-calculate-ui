import styled from "styled-components";

const ButtonC = styled.button`
    width: 76px;
    height: 76px;
    font-size: 20px;
    border: 0.5px solid rgb( 58, 70, 85 );
    color: orange;
    background-color: rgb(64,77,94);
`

export default ButtonC;